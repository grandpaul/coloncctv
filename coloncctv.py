#!/usr/bin/env python3
#    ColonCCTV Player
#    Copyright (c) 2020 Ying-Chun Liu (PaulLiu) <paulliu@debian.org>
#    Copyright (c) 2020 ColonCC Communications Commission
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from enum import Enum
import os
import time
import sys
import argparse
import math
import subprocess
import threading
import pathlib
import queue
import csv
import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstVideo', '1.0')
from gi.repository import GObject, Gst, GLib
import curses
import json
from logzero import logger
import logzero
import dbus
import gc


"""
Use this class to get Youtube's videoURL
"""
class YoutubeDL():
    def __init__(self):
        self.threads = []
        self.id = 0
        pass

    """
    This function gets the video URL.

    Don't use this function. When getting playlist this runs slow to get
    the results.
    That's why we want to use getURLQueue because it creates a thread
    and will return first URL very quickly and download the rest URLs
    in the background.

    @param url: the youtube URL
    @param n: get at most n entries
    @return: the video URL lists
    """
    def getURL(self, url, n=10):
        p1 = subprocess.Popen(args=["youtube-dl",
                                    "-f",
                                    "[height<=480]",
                                    "--get-url", url],
                              stdout=subprocess.PIPE, encoding="utf-8")
        ret = []
        while True:
            line = p1.stdout.readline()
            if not line:
                break
            line1 = line.rstrip()
            ret.append(line1)
            logger.debug("YoutubeDL got Video URL: %s"%(line1))
            if (len(ret) >= n):
                break
        p1.kill()
        p1.wait(10)
        return ret

    """
    Worker function gets the video URL. 

    Don't call this directly. It is to be called internally.

    @param url: the youtube URL
    @param n: get at most n entries
    @param quality: the quality of video in height.
    @param queue1: the queue for receiving URLs
    @param data: private data to store thread infos.
    """
    def getURLQueueWorker(self, url, n, quality, queue1, data):
        logger.debug("YoutubeDL: Thread %d started" % (data["id"]))
        p1 = None
        yargs = [ "youtube-dl" ]
        if (quality):
            yargs.append("-f")
            yargs.append("[height<=%d]"%(int(quality)))
        if (n):
            yargs.append("--playlist-end")
            yargs.append(str(n))
        yargs.append("-i")
        yargs.append("--get-url")
        yargs.append(url)
        p1 = subprocess.Popen(args=yargs,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.DEVNULL,
                              encoding="utf-8")
        i = 0
        while (data["loopFlag"]):
            line = p1.stdout.readline()
            if not line:
                break
            line1 = line.rstrip()
            i = i + 1
            queue1.put(line1)
            logger.debug("YoutubeDL got Video URL: %s"%(line1))
            if ((n and i >= n) or (i >= 100)):
                break
        p1.kill()
        p1.wait(10)
        data["loopFlag"] = False
        logger.debug("YoutubeDL: Thread %d ended" % (data["id"]))
        return False

    """
    This function gets the video URL. 

    This function is using multi-threading to download URL lists.
    The results will be in a queue.

    @param url: the youtube URL
    @param n: get at most n entries
    @param quality: the quality of video in height.
    @return: the queue of video lists.
    """
    def getURLQueue(self, url, n, quality):
        logger.debug("YoutubeDL: Join threads")
        i = 0
        while (i < len(self.threads)):
            if (not self.threads[i]["loopFlag"]):
                self.threads[i]["thread"].join()
                logger.debug("YoutubeDL: Thread %d joined" % (self.threads[i]["id"]))
                self.threads.pop(i)
                continue
            i = i + 1
        logger.debug("YoutubeDL: Join threads Done")
        q = queue.Queue()
        data = {}
        data["loopFlag"] = True
        data["id"] = self.id
        self.id = self.id + 1
        t1 = threading.Thread(target = self.getURLQueueWorker, args = (url, n, quality, q, data, ))
        data["thread"] = t1
        self.threads.append(data)
        t1.start()
        return q

"""
We define an enum to indicate which type of the URL is.

TYPE_YOUTUBER means youtube playlist.
"""
class ChannelDataType(Enum):
    TYPE_NONE = 0
    TYPE_YOUTUBE_LIVE = 1
    TYPE_TEST = 2
    TYPE_YOUTUBER = 3
    TYPE_YOUTUBE_REPLAY = 4
    
class ChannelData():
    def __init__(self, channel="", url="", type=ChannelDataType.TYPE_NONE):
        self.channel = channel
        self.url = url
        self.type = type
        self.onChannelList = False
        self.numOfVideos = None
        self.name = ""
        self.fromConfigFile = ""
        pass

    """
    Set data from JSON data.

    @param jsonData: json data.
    """
    def setDataFromJson(self, jsonData):
        ret = False
        if (not ("channel" in jsonData)):
            return False
        type = ChannelDataType.TYPE_YOUTUBE_LIVE
        if ("type" in jsonData):
            if (jsonData["type"] == "youtube_live"):
                type = ChannelDataType.TYPE_YOUTUBE_LIVE
            elif (jsonData["type"] == "test"):
                type = ChannelDataType.TYPE_TEST
            elif (jsonData["type"] == "youtuber"):
                type = ChannelDataType.TYPE_YOUTUBER
            elif (jsonData["type"] == "youtube_replay"):
                type = ChannelDataType.TYPE_YOUTUBE_REPLAY
            else:
                type = ChannelDataType.TYPE_NONE
        isOnChannelList = self.onChannelList
        if ("onChannelList" in jsonData):
            if (jsonData["onChannelList"] == "yes"
                or jsonData["onChannelList"] == 1
                or jsonData["onChannelList"] == "true"
                or jsonData["onChannelList"] == "1"):
                isOnChannelList = True
        numOfVideos = self.numOfVideos
        if ("numOfVideos" in jsonData):
            numOfVideos = int(jsonData["numOfVideos"])
        if (type == ChannelDataType.TYPE_YOUTUBE_LIVE
            or type == ChannelDataType.TYPE_YOUTUBER
            or type == ChannelDataType.TYPE_YOUTUBE_REPLAY):
            if(not ("url" in jsonData)):
                return False
            self.url = jsonData["url"]
            self.numOfVideos = numOfVideos
            self.channel = jsonData["channel"]
            self.type = type
            self.onChannelList = isOnChannelList
            ret = True
        elif (type == ChannelDataType.TYPE_TEST):
            self.channel = jsonData["channel"]
            self.type = type
            self.onChannelList = isOnChannelList
            ret = True
        if ("name" in jsonData):
            self.name = jsonData["name"]
        return ret

    def getURL(self):
        return self.url

    def getType(self):
        return self.type

    def getChannel(self):
        return self.channel

    def getNumOfVideos(self):
        return self.numOfVideos

    def isOnChannelList(self):
        return self.onChannelList

    def getName(self):
        return self.name

    def setFromConfigFile(self, s):
        self.fromConfigFile = s

    def getFromConfigFile(self):
        return self.fromConfigFile

class ColonCCTVConfig():
    _instance = None
    
    def __init__(self):
        self.channelList = []
        self.channelDict = {}
        self.configDict = {}
        if (ColonCCTVConfig._instance is not None):
            raise Exception('only one instance can exist')
        else:
            ColonCCTVConfig._instance = self
            self.loadConfigFiles()
        pass

    def getInstance():
        if (ColonCCTVConfig._instance is None):
            c1 = ColonCCTVConfig()
        return ColonCCTVConfig._instance

    def getChannelList(self):
        return self.channelList

    def getConfigDict(self):
        return self.configDict

    def getConfig(self, k):
        d = self.getConfigDict()
        if (k in d):
            return d[k]
        return None

    def loadConfigFiles(self):
        paths = [ "./conf.d" ]

        self.channelList = []
        self.channelDict = {}

        for path in paths:
            p = pathlib.Path(path)
            for x in sorted(p.iterdir()):
                if (not x.is_file()):
                    continue
                if (x.name.endswith("~")):
                    continue
                if (not x.name.endswith(".json")):
                    continue
                with open(x) as json_file:
                    try:
                        data = json.load(json_file)
                    except:
                        continue
                    if ("channels" in data):
                        self.loadChannelList(data, x.name)
                    if ("config" in data):
                        self.loadConfigData(data, x.name)

        self.channelList = sorted(self.channelList,
                                  key = lambda x : int(x.getChannel()))

    def loadConfigData(self, jsonData, filename=""):
        if ("config" in jsonData):
            for k in jsonData["config"]:
                self.configDict[k] = jsonData["config"][k]
        
    def loadChannelList(self, jsonData, filename=""):
        if ("channels" in jsonData):
            for channel in jsonData["channels"]:
                c = ChannelData()
                if (c.setDataFromJson(channel)):
                    c.setFromConfigFile(filename)
                    if (not (c.getChannel() in self.channelDict)):
                        self.channelList.append(c)
                        self.channelDict[c.getChannel()] = c
                    else:
                        self.channelList.remove(self.channelDict[c.getChannel()])
                        self.channelList.append(c)
                        self.channelDict[c.getChannel()] = c
                        logger.warning("Channel duplicated: %s" % (c.getChannel()))
        pass
    
class NetworkDetector():
    def __init__(self):
        pass

    def isNetworkAvailable(self):
        ret = False
        
        bus = dbus.SystemBus()

        proxy = bus.get_object("org.freedesktop.NetworkManager",
                               "/org/freedesktop/NetworkManager")
        manager = dbus.Interface(proxy, "org.freedesktop.NetworkManager")

        # Get device-specific state
        devices = manager.GetDevices()
        for d in devices:
            dev_proxy = bus.get_object("org.freedesktop.NetworkManager", d)
            prop_iface = dbus.Interface(dev_proxy,
                                        "org.freedesktop.DBus.Properties")

            # Get the device's current state and interface name
            state = prop_iface.Get("org.freedesktop.NetworkManager.Device",
                                   "State")
            name = prop_iface.Get("org.freedesktop.NetworkManager.Device",
                                  "Interface")

            # and print them out
            if (state == 100):   # activated
                logger.info("Device %s is activated - %d" % (name, state))
            else:
                logger.info("Device %s is not activated - %d" % (name, state))


        # Get active connection state
        manager_prop_iface = dbus.Interface(proxy,
                                            "org.freedesktop.DBus.Properties")
        active = manager_prop_iface.Get("org.freedesktop.NetworkManager",
                                        "ActiveConnections")
        for a in active:
            ac_proxy = bus.get_object("org.freedesktop.NetworkManager", a)
            prop_iface = dbus.Interface(ac_proxy,
                                        "org.freedesktop.DBus.Properties")
            state = prop_iface.Get("org.freedesktop.NetworkManager.Connection.Active",
                                   "State")

            # Connections in NM are a collection of settings that describe
            # everything
            # needed to connect to a specific network.  Lets get those
            # details so we
            # can find the user-readable name of the connection.
            con_path = prop_iface.Get("org.freedesktop.NetworkManager.Connection.Active",
                                      "Connection")
          
            con_service = "org.freedesktop.NetworkManager"

            # ask the provider of the connection for its details
            service_proxy = bus.get_object(con_service, con_path)
            con_iface = dbus.Interface(service_proxy,
                                       "org.freedesktop.NetworkManager.Settings.Connection")
            con_details = con_iface.GetSettings()
            con_name = con_details['connection']['id']
            
            if (state == 2):   # activated
                logger.info("Connection '%s' is activated" % (con_name))
                if ((not con_name.startswith("docker"))
                    and (not con_name.startswith("teredo"))):
                    ret = True
            else:
                logger.info("Connection '%s' is activating" % (con_name))
        return ret
    
class Player():
    def __init__(self):
        self.ydl = YoutubeDL()
        self.pipeline = None
        self.gotoChannel = ""
        self.gotoChannelTimestamp = time.time()
        self.channelList = []
        self.loadChannelList()
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.currentChannel = None
        self.urlQueue = None
        self.mainloop = None
        self.videoQuality = ColonCCTVConfig.getInstance().getConfig("videoQuality")

    def __del__(self):
        try:
            curses.endwin()
        except:
            pass
        pass

    def loadChannelList(self):
        self.channelList = ColonCCTVConfig.getInstance().getChannelList()

    def matchChannel(self, str):
        ret = []
        for channel in self.channelList:
            if (channel.getChannel().startswith(str)):
                ret.append(channel)
        return ret

    def goChannel(self, channel):
        self.currentChannel = channel
        if (self.currentChannel.getType() == ChannelDataType.TYPE_TEST):
            self.urlQueue = None
        else:
            self.urlQueue = self.ydl.getURLQueue(self.currentChannel.getURL(),
                                                 self.currentChannel.getNumOfVideos(),
                                                 self.videoQuality)
        logger.debug("Go to Channel %s" % (self.currentChannel.getChannel()))
        if (self.urlQueue):
            url = None
            try:
                url = self.urlQueue.get(block=True, timeout=10)
            except queue.Empty:
                logger.debug("urlQueue is empty!")
            if (url):
                self.playVideo(self.currentChannel, url)
                self.urlQueue.task_done()
        else:
            self.playVideo(channel, None)

    def playVideo(self, channel, videoURL=None):
        if (videoURL):
            logger.debug("Play Channel %s with videoURL %s" % (channel.getChannel(), videoURL))
        else:
            logger.debug("Play Channel %s with videoURL None" % (channel.getChannel()))
            
        if (self.pipeline):
            bus1 = self.pipeline.get_bus()
            if (bus1):
                bus1.remove_signal_watch()
                del bus1
                bus1 = None
            self.pipeline.set_state(Gst.State.NULL)
            r1 = self.pipeline.get_state(Gst.CLOCK_TIME_NONE)
            logger.debug(r1)
            del self.pipeline
            r1 = gc.collect()
            logger.debug("gc collected %d", r1)
            self.pipeline = None

        if (channel.getType() == ChannelDataType.TYPE_YOUTUBE_LIVE):
            self.pipeline = Gst.parse_launch("souphttpsrc is-live=true location=\""+videoURL+"\" name=src1 ! queue ! decodebin3 name=demuxer  demuxer. ! queue name=video1 ! videoconvert ! textoverlay name=channeltext valignment=2 halignment=2 font-desc=\"normal 22\" text=\"\" wait-text=0 ! autovideosink name=sink1 demuxer.audio_0 ! queue name=audio1 ! audioconvert ! audioresample ! autoaudiosink")
        elif(channel.getType() == ChannelDataType.TYPE_TEST):
            self.pipeline = Gst.parse_launch("videotestsrc name=src1 ! textoverlay name=channeltext valignment=2 halignment=2 font-desc=\"normal 22\" text=\"\" wait-text=0 ! autovideosink name=sink1  audiotestsrc ! queue ! autoaudiosink")
        elif (channel.getType() == ChannelDataType.TYPE_YOUTUBER):
            self.pipeline = Gst.parse_launch("souphttpsrc location=\""+videoURL+"\" name=src1 ! decodebin3 name=demuxer  demuxer. ! queue name=video1 ! videoconvert ! textoverlay name=channeltext valignment=2 halignment=2 font-desc=\"normal 22\" text=\"\" wait-text=0 ! autovideosink name=sink1  demuxer.audio_0 ! queue name=audio1 ! audioconvert ! audioresample ! autoaudiosink")
        elif (channel.getType() == ChannelDataType.TYPE_YOUTUBE_REPLAY):
            self.pipeline = Gst.parse_launch("souphttpsrc location=\""+videoURL+"\" name=src1 ! queue ! decodebin3 name=demuxer  demuxer. ! queue name=video1 ! videoconvert ! textoverlay name=channeltext valignment=2 halignment=2 font-desc=\"normal 22\" text=\"\" wait-text=0 ! autovideosink name=sink1 demuxer.audio_0 ! queue name=audio1 ! audioconvert ! audioresample ! autoaudiosink")

        logger.debug("Pipeline created")
        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message::eos', self.onEos)
        bus.connect('message::error', self.onError)

        #bus.enable_sync_message_emission()
        #bus.connect('sync-message::element', self.onSyncMessage)

        logger.debug("Pipeline start playing")
        self.pipeline.set_state(Gst.State.PLAYING)
        r1 = self.pipeline.get_state(Gst.CLOCK_TIME_NONE)
        logger.debug(r1)
        logger.debug("Pipeline started playing")

    def runMainLoop(self):
        self.mainloop = GLib.MainLoop()
        GLib.timeout_add(300, self.keyEventHandler)
        self.startGLibMainloop()

    def startGLibMainloop(self):
        if (self.mainloop):
            self.mainloop.run()

    def keyEventHandler(self):
        self.stdscr.nodelay(1)
        while True:
            c = self.stdscr.getch()
            self.stdscr.move(0, 0)
            if (len(self.gotoChannel) > 0
                and (time.time() - self.gotoChannelTimestamp) >= 4):
                for channel in self.channelList:
                    if (channel.getChannel() == self.gotoChannel):
                        self.goChannel(channel)
                        # clean rest of the input buffer
                        c1 = self.stdscr.getch()
                        while (c1 != -1):
                            c1 = self.stdscr.getch()
                self.gotoChannel = ""
                self.gotoChannelTimestamp = time.time()
                channeltext = self.pipeline.get_by_name("channeltext")
                if (channeltext):
                    channeltext.set_property("text", "")
            elif (len(self.gotoChannel) > 0):
                mc = self.matchChannel(self.gotoChannel)
                if (len(mc) == 0):
                    self.gotoChannel = ""
                    self.gotoChannelTimestamp = time.time()
                    channeltext = self.pipeline.get_by_name("channeltext")
                    if (channeltext):
                        channeltext.set_property("text", "")
                    pass
                elif (len(mc) == 1):
                    if (mc[0].getChannel() == self.gotoChannel):
                        self.goChannel(mc[0])
                        # clean rest of the input buffer
                        c1 = self.stdscr.getch()
                        while (c1 != -1):
                            c1 = self.stdscr.getch()
                        self.gotoChannel = ""
                        self.gotoChannelTimestamp = time.time()
                        channeltext = self.pipeline.get_by_name("channeltext")
                        if (channeltext):
                            channeltext.set_property("text", "")
                    pass
                pass
            elif (len(self.gotoChannel) == 0):
                if (time.time() - self.gotoChannelTimestamp <= 10):
                    channeltext = self.pipeline.get_by_name("channeltext")
                    if (channeltext and self.currentChannel):
                        channeltext.set_property("text", self.currentChannel.getChannel())
                    pass
                else:
                    channeltext = self.pipeline.get_by_name("channeltext")
                    if (channeltext):
                        p1 = channeltext.get_property("text")
                        if (p1):
                            channeltext.set_property("text", "")
            if (c == -1):
                break
            elif (c == 113):
                self.stop()
                pass
            elif (48 <= c and c <= 57):
                digit = c - 48
                digitS = str(digit)
                self.gotoChannel = self.gotoChannel + digitS
                self.gotoChannelTimestamp = time.time()
                channeltext = self.pipeline.get_by_name("channeltext")
                if (channeltext):
                    channeltext.set_property("text", self.gotoChannel)
                pass
            elif (c == 106):
                if (self.currentChannel):
                    i = 0
                    while (i < len(self.channelList)):
                        if (self.currentChannel.getChannel() == self.channelList[i].getChannel()):
                            break
                        i = i + 1
                    j = 1
                    while (j < len(self.channelList)):
                        if (self.channelList[(i + len(self.channelList) - j) % len(self.channelList)].isOnChannelList()):
                            self.goChannel(self.channelList[(i + len(self.channelList) - j) % len(self.channelList)])
                            self.gotoChannel = ""
                            self.gotoChannelTimestamp = time.time()
                            # clean rest of the input buffer
                            c1 = self.stdscr.getch()
                            while (c1 != -1):
                                c1 = self.stdscr.getch()
                            break
                        j = j + 1
            elif (c == 107):
                if (self.currentChannel):
                    i = 0
                    while (i < len(self.channelList)):
                        if (self.currentChannel.getChannel() == self.channelList[i].getChannel()):
                            break
                        i = i + 1
                    j = 1
                    while (j < len(self.channelList)):
                        if (self.channelList[(i + j) % len(self.channelList)].isOnChannelList()):
                            self.goChannel(self.channelList[(i + j) % len(self.channelList)])
                            self.gotoChannel = ""
                            self.gotoChannelTimestamp = time.time()
                            # clean rest of the input buffer
                            c1 = self.stdscr.getch()
                            while (c1 != -1):
                                c1 = self.stdscr.getch()
                            break
                        j = j + 1
            else:
                logger.info("Unknown key event %d" % (c))
        return True
        
    def stop(self):
        logger.info("Stop")
        if (self.pipeline):
            bus1 = self.pipeline.get_bus()
            if (bus1):
                bus1.remove_signal_watch()
                del bus1
                bus1 = None
            self.pipeline.set_state(Gst.State.NULL)
            del self.pipeline
            self.pipeline = None
        if (self.mainloop):
            self.mainloop.quit()
            del self.mainloop
            self.mainloop = None
        try:
            curses.endwin()
        except:
            pass

    def onSyncMessage(self, bus, msg):
        if (msg.get_structure().get_name() == 'prepare-window-handle'):
            pass

    def decodebinSrcPadCreated(self, element, pad):
        pad_type = pad.get_current_caps().get_structure(0).get_name()
        autovideosink = self.pipeline.get_by_name("video1")
        audioconvert = self.pipeline.get_by_name("audio1")
        audioconvert_sink_pad = audioconvert.get_static_pad("sink")
        autovideosink_sink_pad = autovideosink.get_static_pad("sink")

        if pad_type == "audio/x-raw" and not audioconvert_sink_pad.is_linked():
            pad.link(audioconvert_sink_pad)
            logger.debug("audio/x-raw links to audioconvert_sink_pad")

        if pad_type == "video/x-raw" and not autovideosink_sink_pad.is_linked():
            pad.link(autovideosink_sink_pad)
            logger.debug("video/x-raw links to autovideosink_sink_pad")

    def doSwitch(self, uri):
        # Get ready to add/remove elements.
        self.pipeline.set_state(Gst.State.READY)

        souphttpsrc = self.pipeline.get_by_name("src1")
        decodebin = self.pipeline.get_by_name("demuxer")
        autovideosink = self.pipeline.get_by_name("video1")
        audioconvert = self.pipeline.get_by_name("audio1")

        # Remove old souphttpsrc.
        souphttpsrc.set_state(Gst.State.NULL)
        souphttpsrc.unlink(decodebin)
        self.pipeline.remove(souphttpsrc)
        del souphttpsrc

        # Remove old decodebin.
        decodebin.set_state(Gst.State.NULL)
        decodebin.unlink(autovideosink)
        decodebin.unlink(audioconvert)
        self.pipeline.remove(decodebin)
        del decodebin

        # Reset pipeline time so stream can play from the beginning.
        self.pipeline.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH, 0)

        # Add new decodebin.
        decodebin = Gst.ElementFactory.make("decodebin", "demuxer")
        self.pipeline.add(decodebin)
        decodebin.connect("pad-added", self.decodebinSrcPadCreated)

        # Add new souphttpsrc.
        souphttpsrc = Gst.ElementFactory.make("souphttpsrc", "src1")
        souphttpsrc.set_property("is-live", True)
        souphttpsrc.set_property("location", uri)
        self.pipeline.add(souphttpsrc)
        souphttpsrc.link(decodebin)

        # Play stream.
        self.pipeline.set_state(Gst.State.PLAYING)
        
    def onEos(self, bus, msg):
        logger.info('onEos(): handling EoS')
        if (self.currentChannel and self.currentChannel.getType()==ChannelDataType.TYPE_YOUTUBE_LIVE):
            self.goChannel(self.matchChannel("999")[0])
        elif (self.currentChannel and self.currentChannel.getType()==ChannelDataType.TYPE_TEST):
            self.stop()
        elif (self.currentChannel and self.currentChannel.getType()==ChannelDataType.TYPE_YOUTUBER):
            url = None
            try:
                url = self.urlQueue.get(block=True, timeout=10)
            except queue.Empty:
                url = None
                logger.debug("urlQueue is empty!")
            if (url):
                self.doSwitch(url)
                self.urlQueue.task_done()
            else:
                self.pipeline.seek_simple(Gst.Format.TIME, 
                                          Gst.SeekFlags.FLUSH,
                                          0)
            pass
        elif (self.currentChannel and self.currentChannel.getType()==ChannelDataType.TYPE_YOUTUBE_REPLAY):
            self.pipeline.seek_simple(Gst.Format.TIME, 
                                      Gst.SeekFlags.FLUSH,
                                      0)
            pass
        else:
            self.stop()

    def onError(self, bus, msg):
        logger.error('onError(): ', msg.parse_error())
        self.stop()

def generateTVProgram():
    channelsR = ColonCCTVConfig.getInstance().getChannelList()
    channels = [ x for x in channelsR if x.isOnChannelList() ]
    n = len(channels)
    csvFilename = "tvprogram.csv"

    with open(csvFilename, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
        for i in range(math.ceil(n/3)):
            row1 = []
            for k in range(3):
                if (i*3+k < n):
                    channel = channels[i*3+k]
                    row1.append(int(channel.getChannel()))
                    if (channel.getFromConfigFile() == "10_default_channel.json"):
                        row1.append("%s ☆"%(channel.getName()))
                    else:
                        row1.append(channel.getName())
            writer.writerow(row1)
        
def parseArgs():
    ap = argparse.ArgumentParser()

    ap.add_argument('--generate-tvprogram',
                    action='store_true',
                    help="Generate TV program sheet")
    return vars(ap.parse_args())
        
if __name__ == '__main__':

    args = parseArgs()

    # Set logfile
    logzero.logfile("/tmp/coloncctv.log")

    if (args["generate_tvprogram"]):
        generateTVProgram()
        sys.exit(0)
    
    Gst.init(None)

    defaultChannel = ColonCCTVConfig.getInstance().getConfig("defaultChannel")
    if (not defaultChannel):
        defaultChannel = "52"

    # We wait for 10 seconds for network available
    nd = NetworkDetector()
    for i in range(10):
        a = nd.isNetworkAvailable()
        if (a):
            break
        time.sleep(1)
    
    # Start player
    p = Player()
    
    p.goChannel(p.matchChannel(defaultChannel)[0])
    p.runMainLoop()

    try:
        curses.endwin()
    except:
        pass
    
